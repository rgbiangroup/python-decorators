def foo(function): # 1
    """
    Params:
    ...
    function: reference to the function being decorated
    """
    def bar():
        print("I'm inside the decorator...") # 3
        return function() #4
    return bar # 2

@foo
def my_function():
    print("I'm a function going to be decorated .") ## 5

def split_text(function):

    def wrapper():
        func = function()
        print(func)
        splitted = func.split()
        return splitted
    return wrapper

@split_text
def say_hi():
    text = "Hello, world RBGIANS"
    print(text)
    return text



if __name__ == "__main__":
    x =  say_hi()
    print(x)